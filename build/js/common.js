$(document).ready(function() {

	$(".js-crsl-partners").slick({
		slidesToShow: 5,
		slidesToScroll: 5,
		infinite: true,
		arrows: true,
		prevArrow: '<button class="crsl-arrow arr-prev js-crsl-prev" type="button"><i class="icon-arr-l"></i></button>',
		nextArrow: '<button class="crsl-arrow arr-next js-crsl-next" type="button"><i class="icon-arr-r"></i></button>',
		
	});
	$(".js-crsl-descr").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		asNavFor: '.js-crsl-photo',
		prevArrow: '<button class="crsl-arrow arr-prev js-crsl-prev" type="button"><i class="icon-arr-l"></i></button>',
		nextArrow: '<button class="crsl-arrow arr-next js-crsl-next" type="button"><i class="icon-arr-r"></i></button>'
	});
	$(".js-crsl-photo").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		asNavFor: '.js-crsl-descr',
		fade: true,
		arrows: false
	});
	$(".js-crsl-review").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		prevArrow: '<button class="crsl-arrow arr-prev js-crsl-prev" type="button"><i class="icon-arr-l"></i></button>',
		nextArrow: '<button class="crsl-arrow arr-next js-crsl-next" type="button"><i class="icon-arr-r"></i></button>'
	});
	function initMasonry() {
		$('.js-masonry').masonry({
			itemSelector: '.js-masonry-item',
			columnWidth: '.js-masonry-sizer',
			percentPosition: true
		});

	}
	initMasonry();

    function tabsLoad() {
        var hash = window.location.hash;
        if (hash) {
            $('[href="'+hash+'"]').parents(".js-tabs-group").find(".js-tabs-content").hide();
            $('[data-id="'+hash+'"]').show();
            $('[href="'+hash+'"]').parents(".js-tabs").find("li").removeClass("is-active");
            $('[href="'+hash+'"]').parent().addClass("is-active");
            if ($('[href="'+hash+'"]').data("reinit")) {
                initMasonry();
            }
        }
        else {
            $('.js-tabs').each(function(){
              $(this).find('li:first').addClass("is-active");
              $(this).next().show();
            });
            
        }
        
    }
   tabsLoad();
    $('.js-tabs a').on("click",function () {
        var content = $(this).attr("href");
        $(this).parents(".js-tabs").find("li").removeClass("is-active");
        $(this).parent().addClass("is-active");
        $(this).parents(".js-tabs-group").find(".js-tabs-content").hide();
        $('[data-id="'+content+'"]').show();
        window.location.hash = this.hash;
        if ($(this).data("reinit")) {
            initMasonry()
        }
        return false;
    });
     function tabsLoadIn() {
         var hash = window.location.hash;
         if (hash) {
             $('[href="'+hash+'"]').parents(".js-tabs-group-in").find(".js-tabs-content-in").hide();
             $('[data-id="'+hash+'"]').show();
             $('[href="'+hash+'"]').parents(".js-tabs-in").find("li").removeClass("is-active");
             $('[href="'+hash+'"]').parent().addClass("is-active");
             if ($('[href="'+hash+'"]').data("reinit")) {
                 initMasonry();
             }
         }
         else {
             $('.js-tabs-in').each(function(){
               $(this).find('li:first').addClass("is-active");
               $(this).next().show();
             });
             
         }
         
     }
    tabsLoadIn();
     $('.js-tabs-in a').on("click",function () {
         var content = $(this).attr("href");
         $(this).parents(".js-tabs-in").find("li").removeClass("is-active");
         $(this).parent().addClass("is-active");
         $(this).parents(".js-tabs-group-in").find(".js-tabs-content-in").hide();
         $('[data-id="'+content+'"]').show();
         window.location.hash = this.hash;
         if ($(this).data("reinit")) {
             initMasonry()
         }
         return false;
     });
	$(".js-file-btn").on("click", function (){
	    $(this).next().trigger("click");
	    return false;
	});
    $(".js-file-input").on("change", function (){
        if ($(this).parents(".js-file").data("text")) {
            $(this).parents(".js-file").find(".js-file-name").text(this.files["0"].name);
        }
        else {
            $(this).parents(".js-file").find(".js-file-name").val(this.files["0"].name);
        }
        
    });
    

    var Popup = {
        init: function() {
            this.$el = $(".js-popup");
            this.$btnClose = $(".js-close-popup");
            this.$btnOpen = $(".js-open-popup");
            this.$root = $("html");
            this._bindEvent();
        },
        _bindEvent: function() {
            this.$btnClose.on("click", this._hideWindow.bind(this));
            this.$btnOpen.on("click", this._openWindow.bind(this));
        },
        _hideWindow: function(event) {
            this.$el.removeClass("is-active");
            this.$root.removeClass("is-open-popup");
            return false;
        },
        _openWindow: function(event) {

            var $btn = $(event.target);
            if ($btn[0].nodeName == "I") {
                $btn = $btn.parent();
            }
            else {
                var $btn = $(event.currentTarget);
            }
            this.$popup = $($btn.data("popup"));
            this.index = ""+$btn.data("slide");
            this.$popup.addClass("is-active");
            this.$root.addClass("is-open-popup");
            if (this.index) {
                this._scrollToSlide(this.$popup, this.index)
            }
            return false;
        },
        _scrollToSlide: function(target, index) {
            target.find(".slick-slider").slick("slickGoTo", index);
        }
    }
    Popup.init();
    
    var Select = {
        start: function(options) {
            this._el = document.querySelectorAll(options.selector);
            this._parentClass = options.cssClass;
            this._body = document.body;
            this.init();
        },
        init: function() {
            this._makeDom();
            this._firstLoad();
            this._onClick();
        },
        _makeDom: function() {
            this._createParent();
            this._createList();
            this._createTitle();
        },
        _createParent: function() {
            for (var i = 0; i < this._el.length; i++) {
                var parent = document.createElement("div");
                this._el[i].parentNode.insertBefore(parent, this._el[i]);
                parent.appendChild(this._el[i])
                parent.classList.add(this._parentClass);
                parent.classList.add("sssl");
            }
            this._parent = document.querySelectorAll("." + this._parentClass);
        },
        _createList: function() {
            for (var i = 0; i < this._parent.length; i++) {
                var options = this._parent[i].querySelectorAll("option");
                var ul = document.createElement('ul');
                for (var j = 0; j < options.length; j++) {
                    var li = "<li>" + options[j].innerHTML + "</li>";
                    ul.insertAdjacentHTML("beforeEnd", li);
                }
                this._parent[i].appendChild(ul);
                ul.classList.add(this._parentClass + "__list");
            }
            this._ul = document.querySelectorAll(this._parentClass + " ul");

        },
        _createTitle: function() {
            for (var i = 0; i < this._parent.length; i++) {
                var title = document.createElement("span");
                title.classList.add(this._parentClass + "__head");
                this._parent[i].appendChild(title);
            }
            this._title = document.querySelectorAll(this._parentClass + " span");
        },
        _onClick: function() {
            // for (var i = 0; i < this._parent.length; i++) {
            //     this._parent[i].addEventListener("click", this._clickAction.bind(this));
            // }
            $("body").on("click", ".dropdown", this._clickAction.bind(this));
        },
        _clickAction: function(event) {
            // disabled select
            if (event.currentTarget.children[0].disabled) {
                return false;
            }
            if (event.target.tagName == "SPAN") {
                this._toggleList(event);
            }
            if (event.target.tagName == "LI") {
                this._changeVal(event);
            }
        },
        _toggleList: function(event) {
            event.currentTarget.classList.toggle("is-active");
        },
        _changeVal: function(event) {
            var arrLi = event.currentTarget.querySelectorAll("li");
            var arrOptions = event.currentTarget.querySelectorAll("option");
            var target = event.target;
            var parent = event.currentTarget;
            var index = [].indexOf.call(arrLi, target);
            for (var j = 0; j < arrOptions.length; j++) {
                if (index == j) {
                    arrOptions[j].selected = true;
                    break;
                }
            }

            this._changeTitle(parent, arrLi[index].innerHTML);
            this._changeActive(arrLi, target);
            this._toggleList(event);
        },
        _changeTitle: function(parent, text) {
            parent.querySelector("span").innerHTML = text;
        }, 
        _changeActive: function(arr, target) {
            for (var i = 0; i < arr.length; i++) {
                arr[i].classList.remove("is-active");
            }
            
            target.classList.add("is-active");
        },
        _firstLoad: function() {
            for (var i = 0; i < this._parent.length; i++) {
                var options = this._parent[i].querySelectorAll("option");
                var li = this._parent[i].querySelectorAll("li");
                var arrLi = [].slice.call(li);
                var title = this._parent[i].querySelector("span");
                for (var j = 0; j < options.length; j++) {
                    var option = options[j];
                    if (option.selected) {
                        var optionIndex = j;
                        break;
                    }
                }
                title.innerHTML = arrLi[optionIndex].innerHTML;
            }
        }
    }
    Select.start({
        selector: ".js-select",
        cssClass: "dropdown"
    });

    var slider = $(".js-crsl-success");
    var progress = $(".js-slider-progress span");
    var slidesNumber = slider.find(">div").length;
    var oneSlideProgress = 100/slidesNumber;
    progress.css({
      width:  oneSlideProgress+"%"
    });
    slider.on('init', function(slick) {
          setTimeout(function(){
            $(".crsl-success").addClass("is-ready");
          },200);
    });
    slider.slick({
        autoplay: true,
        autoplaySpeed: 5000,
        slidesToScroll: 1,
        slidesToShow: 1,
        fade: true,
        arrows: true,
        pauseOnHover: false,
        adaptiveHeight: true,
        prevArrow: '<button class="crsl-arrow arr-prev js-crsl-prev" type="button"><i class="icon-arr-t"></i></button>',
        nextArrow: '<button class="crsl-arrow arr-next js-crsl-next" type="button"><i class="icon-arr-b"></i></button>'
    });
    slider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
      progress.css({
        width: oneSlideProgress*(nextSlide+1) + "%"
      });
    });
    // slider.on('afterChange', function(event, slick, currentSlide, nextSlide){
    //   animateNumber(".slick-active .js-animate-number", 4500);
    // });

    // function animateNumber(selector, duration) {
    //     var $el = $(selector);
    //     var number = $el.data("number");
    //     var i = 0;
    //     var timeout = duration/number;
    //     setInterval(function() {
    //         if (i <= number) {
    //             $el.text(i);
    //         }
    //         i++;
    //     }, timeout);
    // }
    // animateNumber(".slick-active .js-animate-number", 4500);

    $(document).on("click", function() {
        if ($(".js-share").hasClass("is-active")) {
            $(".js-share").removeClass("is-active");
        }
    });
    $(".js-share-more").on("click", function() {
        $(this).parents(".js-share").addClass("is-active");
        return false;
    });

  
    $.uploadPreview({
        input_field: "#image-upload",   // Default: .image-upload
        preview_box: "#image-preview",  // Default: .image-preview
        label_field: "#image-label",    // Default: .image-label
        label_default: "загрузить фото",   // Default: Choose File
        label_selected: "загрузить новое",  // Default: Change File
        no_label: false                 // Default: false
      });

    var FieldMaker = {
        init: function() {
            this.$btn = $(".js-add-field");
            this.$btnDelete = $(".js-delete-field");
            this.$btnEdit = $(".js-edit-field");
            this.fieldCount = 1;
            this._bindEvent();
        },
        _bindEvent: function() {
            this.$btn.on("click", this._addField.bind(this));
            this.$btnDelete.on("click", this._deleteField.bind(this));
            this.$btnEdit.on("click", this._editField.bind(this));
        },
        _addField: function(event) {
            var $btn = $(event.currentTarget);
            if ($btn.data("field-count")) {
               var fieldCount = $btn.data("field-count"); 
            }
            else {
                var fieldCount = this.fieldCount; 
            }
            var $newField = $($btn.data("field"));
            $newField.find("input").each(function() {
                var name = $(this).attr("name");
                name = name + "-" + fieldCount;
                $(this).attr("name", name);
            });
            var newFieldHtml = $newField.html();
            $(newFieldHtml).insertBefore('[data-field="'+$btn.data("field")+'"]');
            this.fieldCount++;
        },
        _deleteField: function(event) {
            var $btn = $(event.currentTarget);
            $btn.parents(".js-field").remove();
        },
        _editField: function(event) {
            var $btn = $(event.currentTarget);
            $btn.parents(".js-field").addClass("is-active");
        }
    }
    FieldMaker.init();
   
   $(".js-toggle-password").on("click", function() {
    if ($(this).hasClass("is-active")) {
        $(this).removeClass("is-active");
        $(".js-password").attr("type", "password");
    }
    else {
        $(this).addClass("is-active");
        $(".js-password").attr("type", "text");
    }
   });
   $(".js-dropdown > a").on("click", function() {
        
        if ($(this).parent().hasClass("is-active")) {
            $(".js-dropdown").removeClass("is-active");
        }
        else {
            $(".js-dropdown").removeClass("is-active");
            $(this).parent().addClass("is-active");
        }
        
        return false;
   });


    setTimeout(function() {
        if ($(".out").height() >= 2 * $(window).height()) {
            $(".js-scrolltop").addClass("is-visible");
        }
        else {
            $(".js-scrolltop").removeClass("is-visible");
        }
    }, 500) 
   $(".js-scrolltop").on("click", function() {
        $('html, body').animate({
            scrollTop: 0
        }, 300);
        return false;
   });

    $(".js-validation").each(function() {
        $(this).validate({
            errorClass: "is-invalid",
            validClass: "is-valid",
            errorElement: "span",
            rules: {
                email: {
                    email: true
                }
            },
            messages: {
                email: {
                    email: "заполнено неверно"
                }
            },
            invalidHandler: function(event, validator) {
                // 'this' refers to the form
                var errors = validator.numberOfInvalids();
                if (errors) {
                    $(this).addClass("is-invalid");
                } else {
                  $(this).removeClass("is-invalid");
                }
            }
        });
    })
    $.extend($.validator.messages, {
        required: "обязательно к заполнению"
    });
});